require 'rspec'
require 'card'

describe Card do

  let(:suits) { [:spade, :club, :diamond, :heart] }

  context "when creating card" do
    let(:spade_card) { Card.new(:spade, 5) }
    let(:heart_card) { Card.new(:heart, 13) }

    it "has a traditional card suit" do
      expect(suits).to include(spade_card.suit)
    end

    it "gets suit from constructor" do
      expect(spade_card.suit).to eq(:spade)
      expect(heart_card.suit).to eq(:heart)
    end

    it "gets value from constructor" do
      expect(spade_card.value).to eq(5)
      expect(heart_card.value).to eq(13)
    end

    it "begins face down" do
      expect(spade_card.face_up?).to_not be
    end
  end

  context "when creating cards with standard factory" do

    let(:standard_card_set) { Card::standard_deck }

    it "has 52 cards" do
      expect(standard_card_set.length).to eq(52)
      expect(standard_card_set).to all(be_a(Card))
    end

    it "has no repeating cards" do
      expect(standard_card_set.uniq.length).to eq(52)
    end

  end

  subject(:card1) { Card.new(:diamond, 5) }
  subject(:card2) { Card.new(:heart, 3) }
  subject(:card3) { Card.new(:spade, 5) }

  it "compares self to other cards by value" do
    expect(card1 <=> card2).to eq(1)
    expect(card2 <=> card3).to eq(-1)
    expect(card1 <=> card3).to eq(0)

    expect(card1 == card3).to be
  end
end
