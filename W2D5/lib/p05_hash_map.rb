require_relative 'p02_hashing'
require_relative 'p04_linked_list'

class HashMap
  include Enumerable
  attr_reader :count

  def initialize(num_buckets = 8)
    @store = Array.new(num_buckets) { LinkedList.new }
    @count = 0
  end

  def include?(key)
    return false unless bucket(key).get(key)
    true
  end

  def set(key, val)
    # raise "already in Hash" if include?(key)
    resize! if count >= num_buckets
    if bucket(key).get(key)
      bucket(key).remove(key)
      @count -= 1
    end
    bucket(key).insert(key, val)
    @count += 1
  end

  def get(key)
    bucket(key).get(key)
  end

  def delete(key)
    bucket(key).remove(key)
    @count -= 1
  end

  def each (&prc)
    all_items = []
    @store.each do |ll|
       ll.each do |item|
        prc.call(item.key, item.val)
      end
    end
    self
  end

  def to_s
    pairs = inject([]) do |strs, (k, v)|
      strs << "#{k.to_s} => #{v.to_s}"
    end
    "{\n" + pairs.join(",\n") + "\n}"
  end

  alias_method :[], :get
  alias_method :[]=, :set

  private

  def num_buckets
    @store.length
  end

  def resize!
    new_link_arr = []
    self.each { |k, v| new_link_arr << [k, v] }
    new_resize = Array.new(num_buckets * 2) { LinkedList.new }
    @store = new_resize
    @count = 0
    new_link_arr.each do |i|
      self.set(i[0],i[1])
    end


  end

  def bucket(key)
     @store[key.hash % num_buckets]
  end
end
