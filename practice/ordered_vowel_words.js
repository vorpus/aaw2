
function ordered_word (arr) {
  var vowels = [];
  var broken = arr.split('');
  for (var i = 0; i < broken.length; i++) {
    if (broken[i] == 'a' || broken[i] == 'e' || broken[i] == 'i' || broken[i] == 'o' || broken[i] == 'u' ) {
      vowels.push(broken[i]);
    }
  }
  for (var i = 0; i < vowels.length-1; i++) {
    if (!(vowels[i] < vowels[i+1])) {
      return false;
    }
  }
  return true;
}

function ordered_vowel_words (phrase) {
  newphrase = "";
  words = phrase.split(' ');
  for (var i = 0; i < words.length; i++) {
    if (ordered_word(words[i])) {
      newphrase += words[i];
      newphrase += " ";
    }
  }
  return newphrase;
}

console.log(ordered_vowel_words('"this is a test of the vowel ordering system"'))
