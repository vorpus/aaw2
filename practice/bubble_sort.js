function bubble_sort(arr) {
  var newarr = arr;
  var sorted = false;
  while (!sorted) {
    sorted = true;
    for (var i = 0; i < newarr.length - 1; i++) {
      if (newarr[i] > newarr[i + 1]) {
        var temp = newarr[i];
        newarr[i] = newarr[i + 1];
        newarr[i + 1] = temp;
        sorted = false;
      }
    }
  }
  return newarr;
}

console.log(bubble_sort([1,3,2,346,326,1346,1,612]))
