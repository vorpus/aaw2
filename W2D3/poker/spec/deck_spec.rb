require 'rspec'
require 'deck'

describe Deck do

  subject(:main_deck) { Deck::fresh_deck }

  context "when creating a deck" do

    it "has 52 cards" do
      expect(main_deck.size).to eq(52)
      expect(main_deck.cards).to all(be_a(Card))
    end

    it "has no repeating cards" do
      expect(main_deck.cards.uniq.length).to eq(52)
    end
  end

  context "when dealing cards from a shuffled deck" do

    it "returns number of cards requested" do
      expect(main_deck.deal!(3).length).to eq(3)
    end

    it "subtracts cards from deck when dealing" do
      main_deck.deal!(3)
      expect(main_deck.size).to eq(49)
    end

    it "deals cards not in default order" do
      dealt = main_deck.deal!(4)
      dealt_value = dealt.map {|card| card.value}
      expect(dealt_value).to_not eq([13, 12, 11, 10])
    end
  end
end
