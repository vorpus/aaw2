require_relative 'piece.rb'
require_relative 'display.rb'
require 'byebug'

STARTING_SPOTS = {
:pawn_spots => [[1,0], [1,1], [1,2], [1,3], [1,4], [1,5], [1,6], [1,7],
              [6,0], [6,1], [6,2], [6,3], [6,4], [6,5], [6,6], [6,7]],
:castle_spots => [[0,0], [7,0], [0,7], [7,7]],
:knight_spots => [[0,1], [7,1], [0,6], [7,6]],
:bishop_spots => [[0,2], [7,2], [0,5], [7,5]],
:queen_spots => [[7,3], [0,3]],
:king_spots => [[7,4], [0,4]]
}

class Board
  attr_accessor :grid

  def initialize(grid = nil)
    @grid ||= Array.new(8) {Array.new(8) { NullPiece.new } }
    @rendered = Display.new([0,0], self)
  end

  def new_board_copy

    mock_board = Board.new
    self.grid.each_with_index do |row, row_idx|
      row.each_with_index do |piece, piece_idx|
        if piece.class == Pawn
          mock_board[[row_idx, piece_idx]] = piece.class.new(piece.color, mock_board, piece.direction)
        elsif piece.class == NullPiece
          mock_board[[row_idx, piece_idx]] = piece.class.new
        else
          mock_board[[row_idx, piece_idx]] = piece.class.new(piece.color, mock_board)
        end
      end
    end
    mock_board

  end

  def [](pos)
    x,y = pos
    @grid[x][y]
  end

  def []=(pos, val)
    x,y = pos
    @grid[x][y] = val
  end

  def populate_board
    STARTING_SPOTS.keys.each do |piece|
      STARTING_SPOTS[piece].each do |pos|
        if pos[0] > 4
          self[pos] = pick_white_piece(piece)
        else
          self[pos] = pick_black_piece(piece)
        end
      end
    end
  end

  def pick_black_piece(piece)
    color = :black
    case piece
    when :pawn_spots
      Pawn.new(color, self, :down)
    when :castle_spots
      Castle.new(color, self)
    when :knight_spots
      Knight.new(color, self)
    when :bishop_spots
      Bishop.new(color, self)
    when :queen_spots
      Queen.new(color, self)
    when :king_spots
      King.new(color, self)
    end
  end

  def pick_white_piece(piece)
    color = :light_white
    case piece
    when :pawn_spots
      Pawn.new(color, self, :up)
    when :castle_spots
      Castle.new(color, self)
    when :knight_spots
      Knight.new(color, self)
    when :bishop_spots
      Bishop.new(color, self)
    when :queen_spots
      Queen.new(color, self)
    when :king_spots
      King.new(color, self)
    end
  end

  def move_piece(start_pos, end_pos)
    if self[start_pos].class == Pawn
      self[start_pos].first_move = false
    end
    self[end_pos] = self[start_pos]
    self[start_pos] = NullPiece.new

  end

  def check_move(start_pos, end_pos)
    #within range of Board
    unless end_pos[0].between?(0,7) && end_pos[1].between?(0,7)
      raise "Your end pos: #{end_pos} is out of bounds"
    end

    if start_pos.class == NullPiece
      raise "There is no piece here"
    end

    #valid move of piece

    #if piece present at end_pos - must be enemy piece
  end


  def self.in_bounds?(pos)
    pos[0].between?(0,7) && pos[1].between?(0,7)
  end

  def render
    @rendered.render
  end

  def in_check?(color)
    opposing_moves = generate_opposing_moves(color)
    own_king_pos = get_king_pos(color)
    opposing_moves.include?(own_king_pos)
  end

  def in_check_mate?(color)
    our_pieces = []
    @grid.each_with_index do |row, idx1|
      row.each_with_index do |piece, idx2|
        if piece.color == color
          current_piece_moves = piece.moves([idx1, idx2])
          current_piece_moves.each do |move|
            mock_board = new_board_copy
            mock_board.move_piece([idx1, idx2], move)
            return false if !mock_board.in_check?(color)
          end # each do
        end # if piece.color == color
      end #row.each_with_index
    end
    true
  end

  def generate_opposing_moves(color)
    all_opposing_moves = []
    own_king_pos = nil
    self.grid.each_with_index do |row, row_idx|
      row.each_with_index do |piece, piece_idx|
        if piece.class == King && piece.color == color
          own_king_pos = [row_idx, piece_idx]
        end
        if piece.class != NullPiece && piece.color != color
          all_opposing_moves.concat(piece.moves([row_idx, piece_idx]))
        end
      end
    end
    all_opposing_moves
  end

  def get_king_pos(color)
    own_king_pos = nil
    self.grid.each_with_index do |row, row_idx|
      row.each_with_index do |piece, piece_idx|
        if piece.class == King && piece.color == color
          own_king_pos = [row_idx, piece_idx]
        end
      end
    end
    own_king_pos
  end

  def get_move(color)
    begin
    start_pos = @rendered.move_cursor.dup
    unless self[start_pos].color == color
      raise "You can't move that!"
    end
    @rendered.pathing(self[start_pos].moves(start_pos))
    end_pos = @rendered.move_cursor
    @rendered.clear_path
    self.move_piece(start_pos, end_pos) if self[start_pos].moves(start_pos).include?(end_pos)
    p "Black: #{in_check?(:black)} | White: #{in_check?(:light_white)}"
    p "Black: #{in_check_mate?(:black)} | White: #{in_check_mate?(:light_white)}"
    render


    rescue => e
      p "Invalid move! #{e}"
      retry
    end
  end

end
