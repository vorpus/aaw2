def first_anagram?(string1, string2)
  string2.chars.permutation.map{|el| el.join("")}.include?(string1)
end

#Time complexity of first anagram is O(n! * n)

def second_anagram?(string1, string2)

  string1.chars.each do |char|
    string2.length.times do
      if string2.include?(char)
        string1.delete!(char)
        string2.delete!(char)
      end
    end
  end
  string1.empty? && string2.empty?
end
# second anagram is time complexity O(n^3)

def third_anagram?(string1, string2)
  first = string1.chars.sort.join("")
  second = string2.chars.sort.join("")
  first == second
end
# third anagram is time complexity O(nlgn)

def fourth_anagram?(string1, string2)
  hash1 = Hash.new(0)

  string1.chars.each do |char|
    hash1[char] += 1
  end

  string2.chars.each do |char|
    hash1[char] -= 1
  end
  hash1.values.all? { |el| el == 0 }
end
# fourth anagram is time comlexity O(n)

if __FILE__ == $PROGRAM_NAME
p first_anagram?("elvis", "lives")
p first_anagram?("gizmo", "sally")

p second_anagram?("elvis", "lives")
p second_anagram?("gizmo", "sally")

p third_anagram?("elvis", "lives")
p third_anagram?("gizmo", "sally")

p fourth_anagram?("elvis", "lives")
p fourth_anagram?("gizmo", "sally")

end
