module SlidingPiece
  
  def generate_moves(start_pos, directions)
    possible_moves = [start_pos]
    directions.each do |dir|
      new_position = [start_pos[0]+dir[0], start_pos[1]+dir[1]]
      while Board.in_bounds?(new_position)
        unless @board[new_position].color == self.color
          possible_moves << new_position
          break if @board[new_position].color
          new_position = [new_position[0]+dir[0], new_position[1]+dir[1]]
        else
          break
        end
      end
    end
    possible_moves.drop(1)
  end

end
