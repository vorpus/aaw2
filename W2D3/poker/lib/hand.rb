class Hand

  def initialize
    @cards = []
  end

  def add_card(cards)
    @cards += cards
  end

  def reveal
    @cards
  end

  def discard(cards)
    @cards -= cards
  end

  def complete?
    @cards.length == 5
  end

  def pair?
    value_hash.values.include?(2)
  end

  def two_pair?
    value_hash.values.count { |el| el == 2} == 2
  end

  def trips?
    value_hash.values.include?(3)
  end

  def quads?
    value_hash.values.include?(4)
  end

  def straight?
    vals = value_hash.values
    keys = value_hash.keys
    vals.all? {|el| el == 1 } && keys.max - keys.min == 4
  end

  def full_house?
    pair? && trips?
  end

  def flush?
    @cards.all? {|card| card.suit == @cards.first.suit}
  end

  def straight_flush?
    straight? && flush?
  end

  def royal_flush?
    straight_flush? && value_hash.keys.max == 13
  end

  def value_hash
    value_hash = Hash.new(0)
    @cards.each do |card|
      value_hash[card.value] += 1
    end
    value_hash
  end

end
