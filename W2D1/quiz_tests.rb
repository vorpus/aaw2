class TestClassy
  attr_accessor :hey
  def initialize(hey)
    @hey = hey
  end
end
t = TestClassy.new([1,2,3])
y = t.dup

p t.hey
p y.hey

t.hey[2] = :hello

p t.hey
p y.hey
