function letter_count(str) {
  output = {};
  letters = str.split('');
  for (var i = 0; i < letters.length; i++) {
    if (!output[letters[i]]) {
      output[letters[i]] = 1;
    }
    else {
      output[letters[i]] += 1;
    }
  }
  console.log(output)
  return output;
}

letter_count("cat")
letter_count("moon")
letter_count("cats are fun")
