class MyQueue

  def initialize
    @store = []
  end

  def enqueue(el)
    @store << el
  end

  def dequeue
    @store.shift
  end

  def peek
    @store.first
  end

  def size
    @store.length
  end

  def empty?
    @store.empty?
  end

end

class MyStack
  attr_accessor :greatest_array, :smallest_array
  def initialize
    @store = []
    @greatest_array = []
    @smallest_array = []
  end

  def pop
    if peek == @greatest_array.last
      @greatest_array.pop
    end
    if peek == @smallest_array.last
      @smallest_array.pop
    end
    @store.pop
  end

  def push(el)
    if @greatest_array.empty? || el > @greatest_array.last
      @greatest_array << el
    end
    if @smallest_array.empty? || el < @smallest_array.last
      @smallest_array << el
    end
    @store.push(el)
  end

  def peek
    @store.last
  end

  def size
    @store.length
  end

  def empty?
    @store.empty?
  end

  def max
    @greatest_array.last
  end

  def min
    @smallest_array.last
  end
end

class StackQueue
  attr_accessor :store1, :store2

  def initialize
    @store1 = MyStack.new
    @store2 = MyStack.new
  end

  def enqueue(el)
    @store1.push(el)
  end

  def dequeue
    if @store2.empty?
      until @store1.empty?
        @store2.push(@store1.pop)
      end
    end
    @store2.pop
  end

  def size
    @store1.size + @store2.size
  end

  def empty?
    @store1.empty? && @store2.empty?
  end

  def min
    return nil if @store1.empty? && @store2.empty?
    return @store1.min if @store2.empty?
    return @store2.min if @store1.empty?
    case @store1.min > @store2.min
    when true
      @store2.min
    when false
      @store1.min
    end
  end

  def max
    return nil if @store1.empty? && @store2.empty?
    return @store1.max if @store2.empty?
    return @store2.max if @store1.empty?
    case @store1.max < @store2.max
    when true
      @store2.max
    when false
      @store1.max
    end
  end


end




if __FILE__ == $PROGRAM_NAME
  stack = StackQueue.new
  stack.enqueue(3)
  stack.enqueue(7)
  stack.enqueue(2)
  stack.enqueue(5)
  stack.enqueue(4)
  stack.enqueue(8)
  p stack.max # 8
  p stack.min # 2
  stack.dequeue
  p stack.max #8
  p stack.min #2
  stack.dequeue
  stack.dequeue
  stack.dequeue
  p stack.min # 4
  stack.dequeue
  p stack.max # 8
  p stack.min # 8
  stack.dequeue
  p stack.max #nil
  p stack.min #nil
end
