require_relative 'slidingpiece.rb'
require_relative 'steppingpiece.rb'

class Piece
  attr_accessor :color

  def initialize(color, board)
    @color = color
    @board = board
  end

  def to_s
    "♟ ".colorize(@color)
  end



end

class NullPiece < Piece

  def initialize
    @color = nil
  end

  def to_s
    "  "
  end

end

class Castle < Piece

  include SlidingPiece

  def to_s
    "♜ ".colorize(@color)
  end

  def moves(start_pos)
    directions = [[1,0], [-1,0], [0,1], [0,-1]]
    generate_moves(start_pos, directions)
  end
end

class Bishop < Piece

  include SlidingPiece

  def to_s
    "♝ ".colorize(@color)
  end

  def moves(start_pos)
    directions = [[1,1], [-1,-1], [-1,1], [1,-1]]
    generate_moves(start_pos, directions)
  end

end

class Queen < Piece

  include SlidingPiece

  def to_s
    "♛ ".colorize(@color)
  end

  def moves(start_pos)
    directions = [[1,0], [-1,0], [0,1], [0,-1], [1,1], [-1,-1], [-1,1], [1,-1]]
    generate_moves(start_pos, directions)
  end

end

class Knight < Piece

  include SteppingPiece

  def to_s
    "♞ ".colorize(@color)
  end

  def moves(start_pos)
    directions = [[-2, -1], [-2,  1], [-1, -2], [-1,  2], [ 1, -2], [ 1,  2], [ 2, -1], [ 2,  1]]
    generate_moves(start_pos, directions)
  end

end

class King < Piece

  include SteppingPiece

  def to_s
    "♚ ".colorize(@color)
  end

  def moves(start_pos)
    directions = [[1,0], [-1,0], [0,1], [0,-1], [1,1], [-1,-1], [-1,1], [1,-1]]
    generate_moves(start_pos, directions)
  end

end

class Pawn < Piece

  attr_accessor :first_move, :direction

  def initialize(color, board, direction)
    @first_move = true
    @direction = direction
    super(color, board)
  end

  def to_s
    "♟ ".colorize(@color)
  end

  def moves(start_pos)
    if @direction == :down
      directions = [[1, 0]]
      attack_directions = [[1, 1], [1, -1]]
    else
      directions = [[-1, 0]]
      attack_directions = [[-1, -1], [-1, 1]]
    end
    generate_moves(start_pos, directions) + generate_attacks(start_pos, attack_directions)
  end

  def generate_moves(start_pos, directions)
    possible_moves = [start_pos]
    directions.each do |dir|
      new_position = [start_pos[0]+dir[0], start_pos[1]+dir[1]]
      @first_move ? move_times = 2 : move_times = 1
      move_times.times do
        unless @board[new_position].color == self.color
          break if @board[new_position].color
          possible_moves << new_position
          new_position = [new_position[0]+dir[0], new_position[1]+dir[1]]
        else
          break
        end
      end
    end
    possible_moves.drop(1)
  end

  def generate_attacks(start_pos, directions)
    possible_moves = [start_pos]
    directions.each do |dir|
      new_position = [start_pos[0]+dir[0], start_pos[1]+dir[1]]
      if Board.in_bounds?(new_position)
        unless @board[new_position].color == self.color
          possible_moves << new_position if @board[new_position].color
        end
      end
    end
    possible_moves.drop(1)
  end

end
