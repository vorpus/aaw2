class Fixnum
  # Fixnum#hash already implemented for you
end

class Array
  def hash
    return 13 if self.empty?
    final_hash = 0
    self.each_with_index do |i, idx|
      i = i.hash unless i.is_a?(Fixnum)
      final_hash += i * 3 * (idx + 1) - 100 % 79
    end
    final_hash
  end
end

class String
  def hash
    final_hash = 0
    self.each_char.with_index do |c, idx|
      final_hash += (c.ord * (idx + 1))/2 - 18 * (idx + 1)
    end
    final_hash
  end
end

class Hash
  # This returns 0 because rspec will break if it returns nil
  # Make sure to implement an actual Hash#hash method
  def hash
    final_hash = 0
    self.each do |k, v|
      watever_hash = (k.to_s.ord + v.ord) % 13 ^ 5
      final_hash += watever_hash
    end
    final_hash
  end
end
