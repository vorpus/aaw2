class Array

  def my_uniq
    # self.inject(Array.new) {|acc, el| acc << el unless acc.include?(el)}

    selected = []
    self.each do |el|
      selected << el unless selected.include?(el)
    end
    selected
  end

  def two_sum
    unless self.all? { |el| el.is_a?(Integer) }
      raise StandardError("All inputs must be integers")
    end

    pairs = []

    self.each_with_index do |el, i|
      ((i + 1) ... self.length).each do |j|
        pairs << [i, j] if self[i] == -self[j]
      end
    end

    pairs
  end
end

def my_transpose(input)
  raise StandardError("Input must be an array") unless input.is_a? Array
  if input.any? { |el| el.is_a? Array }
    unless input.all? { |el| el.length == input.first.length }
      raise StandardError("Input subarrays must be same length")
    end
  end

  input = [input] unless input.first.is_a? Array

  transposed = Array.new(input.first.length) { Array.new(input.length) }
  transposed.each_index do |i|
    transposed.first.each_index do |j|
      transposed[i][j] = input[j][i]
    end
  end

  transposed
end

def stock_picker(prices)
  unless prices.is_a?(Array) && prices.all? {|p| p.is_a?(Numeric)}
    raise ArgumentError("input must be array of prices")
  end
  raise ArgumentError("no prices in input!") unless prices.length >= 2

  largest_days = [0, 1]

  prices.each_with_index do |price, i|
    (i + 1 ... prices.length).each do |j|
      price_diff = prices[j] - prices[i]
      largest_diff = prices[largest_days[1]] - prices[largest_days[0]]
      if price_diff > largest_diff
        largest_days = [i, j]
      end
    end
  end

  largest_days

end
