class Employee
  attr_reader :salary, :subordinates

  def initialize(name, title, salary, boss)
    @name, @title, @salary, @boss = name, title, salary, boss
    @subordinates = []
  end

  def bonus(multiplier)
    bonus = @salary * multiplier
  end

  def inspect
    @name
  end

end

class Manager < Employee

  def initialize(name, title, salary, boss)
    super(name, title, salary, boss)
  end

  def bonus(multiplier)
    self.find_sub_salaries * multiplier
  end

  def add_subordinate(subordinate)
    @subordinates << subordinate
  end

  def find_sub_salaries
    nodes = [self]
    salaries = []
    until nodes.empty?
      node = nodes.shift
      salaries << node.salary unless node == self
      nodes.concat(node.subordinates)
    end
    salaries.inject(&:+)
  end

end

class Founder < Manager
end

if __FILE__ == $PROGRAM_NAME
  ned = Founder.new("ned", "Founder", 1000000, nil)
  darren = Manager.new("Darren", "TA Manager", 78000, ned)
  ned.add_subordinate(darren)
  david = Employee.new("David", "TA", 10000, darren)
  darren.add_subordinate(david)
  shawna = Employee.new("Shawna", "TA", 12000, darren)
  darren.add_subordinate(shawna)

  p "Ned's subordinates: #{ned.subordinates}"
  p "Darren's subordinates: #{darren.subordinates}"
  
  p "Darren's bonus: #{darren.bonus(4)}"
  p "Ned's bonus: #{ned.bonus(5)}"
end
