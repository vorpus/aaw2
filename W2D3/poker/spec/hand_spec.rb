require 'rspec'
require 'hand'

describe Hand do
  subject(:hand) { Hand.new }
  let(:card1) { double("card1", value: 10, suit: :spade)}
  let(:card2) { double("card2", value: 10, suit: :heart)}
  let(:card3) { double("card3", value: 8, suit: :club)}
  let(:card4) { double("card4", value: 7, suit: :diamond)}
  let(:card5) { double("card5", value: 6, suit: :club)}
  let(:card6) { double("card6", value: 9, suit: :club)}
  let(:card7) { double("card7", value: 13, suit: :club)}
  let(:card8) { double("card8", value: 12, suit: :club)}
  let(:card9) { double("card9", value: 11, suit: :club)}
  let(:card10) { double("card10", value: 10, suit: :club)}
  let(:card11) { double("card11", value: 9, suit: :club)}

  it "can receive and reveal cards" do
    hand.add_card([card1, card2])
    expect(hand.reveal).to eq([card1, card2])
  end

  it "can discard multiple cards" do
    hand.add_card([card1, card2, card3])
    hand.discard([card1])
    expect(hand.reveal).to eq([card2, card3])
  end

  context "when calculating hand value" do

    it "detects a pair" do
      hand.add_card([card1, card2, card3, card4, card5])
      expect(hand.pair?).to be
    end

    it "detects two pair" do
      hand.add_card([card1, card1, card3, card3, card5])
      expect(hand.two_pair?).to be
    end

    it "detects a trips" do
      hand.add_card([card1, card2, card2, card4, card5])
      expect(hand.trips?).to be
    end

    it "detects a straight" do
      hand.add_card([card2, card3, card4, card5, card6])
      expect(hand.straight?).to be
    end

    it "detects a flush" do
      hand.add_card([card3, card3, card5, card6, card7])
      expect(hand.flush?).to be
    end

    it "detects a full house" do
      hand.add_card([card1, card2, card2, card5, card5])
      expect(hand.full_house?).to be
    end

    it "detects a quads" do
      hand.add_card([card1, card2, card2, card2, card5])
      expect(hand.quads?).to be
    end

    it "detects a straight flush" do
      hand.add_card([card7, card8, card9, card10, card11])
      expect(hand.straight_flush?).to be
    end

    it "detects a royal flush" do
      hand.add_card([card7, card8, card9, card10, card11])
      expect(hand.royal_flush?).to be
    end

  end

end
