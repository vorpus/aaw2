class Link
  attr_accessor :key, :val, :next, :prev

  def initialize(key = nil, val = nil)
    @key = key
    @val = val
    @next = nil
    @prev = nil
  end

  def to_s
    "#{@key}: #{@val}"
  end
end

class LinkedList
  include Enumerable
  def initialize
    @head = Link.new
    @tail = Link.new
    @head.next = @tail
    @tail.prev = @head


  end

  def [](i)
    each_with_index { |link, j| return link if i == j }
    nil
  end

  def first
    @head.next
  end

  def last
    @tail.prev
  end

  def empty?
    @head.next == @tail
  end

  def get(key)
    link = @head.next
    until link == @tail
      if link.key == key
        @got_key = link
        return link.val
      end
      link = link.next
    end
    nil
  end

  def include?(key)
    return false unless get(key)
    true
  end

  def insert(key, val)
    new_link = Link.new(key,val)
    new_link.next = @tail
    new_link.prev = @tail.prev
    @tail.prev.next = new_link
    @tail.prev = new_link
  end

  def remove(key)
    get(key)
    got_next = @got_key.next
    got_prev = @got_key.prev
    got_next.prev = got_prev
    got_prev.next = got_next

  end

  def each(&prc)
    link = first
    until link == @tail
      prc.call(link)
      link = link.next
    end

  end


  def to_s
    inject([]) { |acc, link| acc << "[#{link.key}, #{link.val}]" }.join(", ")
  end
end
