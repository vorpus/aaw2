require 'card'

class Deck

  attr_reader :cards

  def self.fresh_deck
    self.new(Card.standard_deck.shuffle!)
  end

  def initialize(cards)
    @cards = cards
  end

  def deal!(how_many)
    deal_cards = []
    how_many.times { deal_cards << @cards.pop }
    deal_cards
  end

  def size
    @cards.length
  end

  def shuffle!
    @cards.shuffle!
  end

end
