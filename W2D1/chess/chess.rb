require_relative 'board.rb'
require_relative 'player.rb'

class Chess
  attr_accessor :board, :current_player

  def initialize(player1, player2)
    @player1, @player2 = player1, player2
    @board = Board.new()
    @board.populate_board
    @current_player = @player1
  end

  def play_turn
    @board.get_move(@current_player.color)
    @current_player == @player1 ? @current_player = @player2 : @current_player = @player1
  end

  def run_game
    until @board.in_check_mate?(@player1.color) || @board.in_check_mate?(@player2.color)
      play_turn
    end
    if @board.in_check_mate?(@player1.color)
      p "#{@player2.name} wins!"
    else
      p "#{@player1.name} wins!"
    end
  end


end


if __FILE__ == $PROGRAM_NAME
  al = Player.new("Al", :light_white)
  jo = Player.new("Joe", :black)
  c = Chess.new(al, jo)
  c.run_game
  # new_board = c.board.new_board_copy
  # new_board.team_pieces(:light_white).map { |el| p el.color }

end
