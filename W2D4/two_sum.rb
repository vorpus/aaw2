def bad_two_sums?(arr, target_sum)

  arr.each_index do |i|
    (i+1...arr.length).each do |j|
      return true if arr[i] + arr[j] == target_sum
    end
  end
  false
end

#Time complexity is O(n^2) worst case

def okay_two_sums?(arr, target_sum)
  arr.sort!

  arr.each_with_index do |el, i|
    return true if bsearch(arr[i+1..-1], target_sum - el)
  end

  false
end
#time complexity of ok 2 sums is O(nlogn)


def bsearch(nums, target)
  return nil if nums.empty?

  probe_index = nums.length / 2
  case target <=> nums[probe_index]
  when -1
    bsearch(nums.take(probe_index), target)
  when 0
    probe_index
  when 1
    sub_answer = bsearch(nums.drop(probe_index + 1), target)
    (sub_answer.nil?) ? nil : (probe_index + 1) + sub_answer
  end

end

def pair_sum?(arr, target)
  elements = Hash.new(0)

  arr.each do |el|
    return true if elements.include?(target - el)
    elements[el] += 1
  end

  false
end

# time complexity of pair_sum is O(n)


if __FILE__ == $PROGRAM_NAME

arr = [0, 1, 5, 7]
p bad_two_sums?(arr, 6) # => should be true
p bad_two_sums?(arr, 10)

p okay_two_sums?(arr, 6) # => should be true
p okay_two_sums?(arr, 10)

p pair_sum?(arr, 6) # => should be true
p pair_sum?(arr, 10)
end
