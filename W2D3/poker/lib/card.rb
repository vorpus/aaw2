class Card

  SUITS = [
    :heart,
    :club,
    :spade,
    :diamond
  ]

  VALUES = (1..13).to_a

  attr_reader :suit, :value

  def self.standard_deck
    deck = []
    SUITS.each do |suit|
      VALUES.each do |value|
        deck << Card.new(suit, value)
      end
    end

    deck
  end

  def initialize(suit, value)
    @suit = suit
    @value = value
    @face_up = false
  end

  def face_up?
    @face_up
  end

  def <=>(next_card)
    self.value <=> next_card.value
  end

  def ==(next_card)
    self.value == next_card.value
  end

end
