class Hanoi
  attr_accessor :towers

  def initialize
    @towers = Array.new(3) {Array.new}
    towers[0] = [5, 4, 3, 2, 1]
  end

  def move(moves)
    from_tower, to_tower = @towers[moves[0]], @towers[moves[1]]
    validate_move(moves)

    unless to_tower.empty?
      raise "Illegal move" if from_tower.last > to_tower.last
      to_tower << from_tower.pop
    else
      to_tower << from_tower.pop
    end
  end

  def validate_move(moves)
    valid_moves = [0,1,2]
    raise "Illegal move" unless (valid_moves - moves).length == 1
  end

  def over?
    [1, 2].any? do |i|
      @towers[i].length == @towers.flatten.length
    end
  end

  def render
    @towers.each_with_index do |tower, i|
      puts "Tower #{i}:   #{tower.join(' ')}"
    end
  end

  def get_move
    the_move = [nil,nil]

    print "\nEnter 'from' tower: "
    the_move[0] = gets.strip.to_i

    print "\nEnter 'to' tower: "
    the_move[1] = gets.strip.to_i

    the_move
  end

  def play
    until over?
      render

      begin
        move(get_move)
      rescue
        retry
      end

    end
  end

end

if __FILE__ == $PROGRAM_NAME
  h = Hanoi.new
  h.play
end
