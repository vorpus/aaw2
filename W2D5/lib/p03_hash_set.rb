require_relative 'p02_hashing'

class HashSet
  attr_reader :count

  def initialize(num_buckets = 8)
    @store = Array.new(num_buckets) { Array.new }
    @count = 0
  end

  def insert(key)
    raise "Already in Hash" if include?(key)
    if @count >= num_buckets
      resize!
    end
    self[key] << key
    @count += 1
  end

  def include?(key)
    self[key].include?(key)
  end

  def remove(key)
    raise "does not exist" unless include?(key)
    self[key].delete(key)
    @count -= 1
  end

  private

  def [](num)
    @store[num.hash % num_buckets]
    # optional but useful; return the bucket corresponding to `num`
  end

  def num_buckets
    @store.length
  end

  def resize!
    flat_store = @store.flatten

    new_resize = Array.new(num_buckets * 2) { Array.new }
    @store = new_resize
    flat_store.each do |i|
      self[i] << i
    end
  end
end
