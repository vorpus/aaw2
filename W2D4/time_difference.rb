require 'byebug'
def my_min1(list)

  (0...list.length).each do |i|
    return list[i] if (i+1...list.length).all? do |j|
    list[i] < list[j]
    end
  end
end
#Time complexity for my_min1 is O(n^2)

def my_min2(list)
  minimum = list.first
  list.each do |el|
    minimum = el if el < minimum
  end
  minimum
end

#Time complexity for my_min1 is O(n)
def sub_sums1(list)
  sub_array = []
  (0...list.length).each do |i|
    (i+1...list.length).each do |j|
      sub_array << list[i..j]
    end
  end

  minimum = []
  sub_array.each do |arr|
    minimum << arr.inject(&:+)
  end
  minimum.max

end
#Time complexity for sub_sums1 is O(n)

def sub_sums2(list)
  # debugger
  i = 0
  greatest = list.first
  until i >= list.length
    leftsum = list[i]
    if leftsum < 0
      greatest = leftsum if leftsum > greatest
      i += 1
      next
    end
    j = i + 1
    until j >= list.length || leftsum + list[j] < 0
      leftsum += list[j]
      greatest = leftsum if greatest < leftsum
      j += 1
    end
      # greatest = leftsum
      i = j + 1
  end
  greatest
end

if __FILE__ == $PROGRAM_NAME
  list = [0, 3, 5, 4, -5, 10, 1, 90]
  list2 = [2, 3, 3, -6, 1, -6,  7]
  list3 = [-5, -1, -3]
  p sub_sums2(list2)
  p sub_sums2(list3)
  # p sub_sums2([-3,4,-5,1,2,5,1,9,-3,-2,-3,11])
end
