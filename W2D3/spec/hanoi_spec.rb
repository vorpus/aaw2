require 'rspec'
require 'hanoi'

describe Hanoi do
  subject(:hanoi_game) { Hanoi.new }

  it "initializes with three towers" do
    expect(hanoi_game.towers.length).to eq(3)
  end

  it "starts with only tower 0 having discs" do
    expect(hanoi_game.towers[0]).to_not be_empty
    expect(hanoi_game.towers[1]).to be_empty
    expect(hanoi_game.towers[2]).to be_empty
  end

  it "initializes with first tower in ordered series" do
    expect(hanoi_game.towers[0]).to eq([5,4,3,2,1])
  end

  context "when moving pieces" do

    it "can move pieces" do
      expect {hanoi_game.move([0, 1])}.to_not raise_error
    end

    it "takes 2D array of positions for move method" do
      expect { hanoi_game.move([0,1]) }.to_not raise_error
    end

    it "ensures moves arguments are valid" do
      expect { hanoi_game.move([0,0]) }.to raise_error
    end

    it "moves an element to empty tower" do
      hanoi_game.move([0,1])
      expect(hanoi_game.towers[0]).to eq([5,4,3,2])
      expect(hanoi_game.towers[1]).to eq([1])
    end

    it "will not make an illegal move" do
      hanoi_game.move([0,1])
      expect { hanoi_game.move([0,1]) }.to raise_error
    end
  end

  context "endgame" do
    it "detects end game state" do
      hanoi_game.towers = [[], [1], [5,4,3,2]]
      hanoi_game.move([1,2])
      expect(hanoi_game.over?).to be
    end

    it "doesn't end game prematurely" do
      hanoi_game.towers = [[], [1], [5,4,3,2]]
      expect(hanoi_game.over?).to_not be
    end
  end
end
