require 'colorize'
require_relative 'cursor.rb'

class Display

  def initialize(cursor_pos = [0, 0], board)
    @cursor_pos = cursor_pos
    @cursor = Cursor.new([0, 0], board)
    @board = board
    @path = []
  end

  def render
    blank_lines
    @board.grid.each_with_index do |row, i|
      print "     "
      row.each_with_index do |cell, j|
        if [i,j] == @cursor_pos
          print "#{cell}".colorize(:background => :red).blink
        elsif @path.include?([i, j])
          print "#{cell}".colorize(:background => :green)
        elsif (i+j)%2 == 0
          print "#{cell}".colorize(:background => :white)
        else
          print "#{cell}".colorize(:background => :light_black)
        end
      end
      print "\n"
    end
    print "\n"
  end

  def pathing(path)
    @path = path
  end

  def clear_path
    @path = []
  end

  def move_cursor
    start = @cursor_pos
    system("clear")
    render
    condition = false
    until condition
      position = @cursor_pos
      @cursor_pos = @cursor.get_input

      if @cursor_pos.nil?
        position = @last_position if position.nil?
        @last_position = position
        return position
      end
     system("clear")
      render
    end
  end

  def blank_lines
    print "\n\n\n"
  end

end
