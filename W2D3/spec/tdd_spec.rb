require 'rspec'
require 'tdd'

describe Array do

  describe "#my_uniq" do

    subject(:kittens) { [1, 2, 3, 4, 5, 1, 1, 4] }
    let(:no_kittens) { [] }

    it "returns an array" do
      expect(kittens.my_uniq).to be_a(Array)
    end

    context "when passed an empty array" do
      it "returns an empty array" do
        expect(no_kittens.my_uniq).to be_empty
      end
    end

    it "returns unique elements of an array" do
      expect(kittens.my_uniq).to eq([1,2,3,4,5])
    end
  end

  describe "#two_sum" do

    let(:two_matches) { [-1, 0, 2, -2, 1] }
    let(:bad_input) { [1, 3.3, 7] }

    it "returns an array" do
      expect(two_matches.two_sum).to be_a(Array)
    end

    it "raises error unless all elements are integers" do
      expect { bad_input.two_sum }.to raise_error
    end

    context "when passed an empty array" do

      let(:no_nums) { [] }

      it "returns an empty array" do
        expect(no_nums.two_sum).to be_empty
      end
    end

    context "when passed an array with a single number" do

      let(:one_num) { [3] }

      it "returns an empty array" do
        expect(one_num.two_sum).to be_empty
      end
    end

    context "when passed an array with no two zero-sum numbers" do

      let(:no_sums) { [-1, 0, 3, -2, 7] }

      it "returns an empty array" do
        expect(no_sums.two_sum).to be_empty
      end
    end

    context "when passed an array with some zero-sum numbers" do

      context "when passed an array with one pair of zero-sum numbers" do

        let(:one_match) { [-1, 0, 2, -8, 1] }

        it "returns an array with index of matching zero-sums" do
          expect(one_match.two_sum).to eq([[0, 4]])
        end
      end

      context "when passed an array with two or more pairs of zero-sum numbers" do
        it "returns an array with index of matching zero-sums" do
          expect(two_matches.two_sum).to eq([[0, 4], [2, 3]])
        end

        context "when passed an array with two or more pairs of zero's" do

          let(:matches_with_zero) { [3, 0, 6, 0, -2, 1, 0] }

          it "returns an array with indicies of pairs of zero's" do
            expect(matches_with_zero.two_sum).to eq([[1, 3], [1, 6], [3, 6]])
          end
        end
      end
    end
  end

end

describe "::my_transpose" do
  let(:two_d) { [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8] ] }
  let(:one_d) { [1, 2, 3, 4] }
  let(:no_d) { "wrong" }
  let(:unbalanced) { [[1,2],[3,4,5]] }


  it "receives an array as an argument" do
    expect { my_transpose(two_d) }.to_not raise_error
    expect { my_transpose(no_d) }.to raise_error
  end

  it "all elements must be same length" do
    expect { my_transpose(unbalanced) }.to raise_error
  end

  it "transposes a 2D array" do
    expect(my_transpose(two_d)).to eq(two_d.transpose)
  end

  it "transposes a 1D array" do
    expect(my_transpose(one_d)).to eq([one_d].transpose)
  end
end

describe "::stock_picker" do
  let(:stock_prices) { [2,4,7,9,6,1] }
  let(:no_prices) { [] }
  let(:one_price) { [7] }
  let(:mangled_input) { ["$56", 750] }
  let(:wrong_input) { "4,5,6" }

  it "receives an array of numbers as an argument" do
    expect { stock_picker(stock_prices) }.to_not raise_error
    expect { stock_picker(wrong_input) }.to raise_error
    expect { stock_picker(mangled_input) }.to raise_error
    expect { stock_picker(one_price) }.to raise_error
  end

  it "outputs array of most profitable buy/sell days" do
    expect(stock_picker(stock_prices)).to eq([0, 3])
  end
end
