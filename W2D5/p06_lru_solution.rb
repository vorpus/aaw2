require_relative 'p05_hash_map'
require_relative 'p04_linked_list'

class LRUCache
  attr_reader :count
  def initialize(max, prc)
    @map = HashMap.new
    @store = LinkedList.new
    @max = max
    @prc = prc
  end

  def count
    @map.count
  end

  def get(key)
    if @map.get(key)
      # p "exists, returning"
      val = @map.get(key)
      update_link!(val)
      val.val
    else
      calc!(key)
    end
  end

  def to_s
    "Map: " + @map.to_s + "\n" + "Store: " + @store.to_s
  end

  private

  def calc!(key)
    val = @prc.call(key)
    @store.insert(key, val)
    new_link = @store.insert(key, val)

    @map.set(key, new_link)
    if count > @max
      eject!
    end
    val
    # suggested helper method; insert an (un-cached) key
  end

  def update_link!(link)
    @store.remove(link.key)
    @store.insert(link.key, link.val)
    # suggested helper method; move a link to the end of the list
  end

  def eject!
    key_first = @store.first.key
    @store.remove(key_first)
    @map.delete(key_first)
  end
end
