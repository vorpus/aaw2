function no_repeats(start, end) {
  years = [];
  for(var i = start; i <= end; i++) {
    if (!repeating(i)) {
      years.push(i);
    }
  }
  console.log(years)
}

function repeating(year) {
  end = false;
  repeat = year.toString().split('');
  for (var i = 0; i < repeat.length; i++) {
    for (var j = i+1; j < repeat.length; j++) {
      if (repeat[i] == repeat[j]) {
        end = true;
      }
    }
  }
  return end;
}

no_repeats(1234, 1234)
no_repeats(1123, 1123)
no_repeats(1980, 1987)
