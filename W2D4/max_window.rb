require_relative 'stacks_and_queues'

def windowed_max_range(arr, window_size)

  current_max_range = nil

  arr.each_index do |i|
    break if i + window_size > arr.length
    test_arr = arr[i ... (i + window_size)]
    range = test_arr.max - test_arr.min
    if current_max_range.nil? || range > current_max_range
      current_max_range = range
    end
  end
  current_max_range
end
#windowed max range naive implementation is time complexity O(n^2)

def better_max_range(arr, window_size)
  stackqueue = StackQueue.new

  window_size.times do |i|
    stackqueue.enqueue(arr[i])
  end
  current_max_range = stackqueue.max - stackqueue.min

  (window_size...arr.length).each do |j|
    stackqueue.dequeue
    stackqueue.enqueue(arr[j])
    this_max_range = stackqueue.max - stackqueue.min
    current_max_range = this_max_range if this_max_range > current_max_range
  end
  current_max_range

end






if __FILE__ == $PROGRAM_NAME
  p better_max_range([1, 0, 2, 5, 4, 8], 2)  == 4 # 4, 8
  p better_max_range([1, 0, 2, 5, 4, 8], 3)  == 5 # 0, 2, 5
  p better_max_range([1, 0, 2, 5, 4, 8], 4)  == 6 # 2, 5, 4, 8
  p better_max_range([1, 3, 2, 5, 4, 8], 5)  == 6 # 3, 2, 5, 4, 8
end
